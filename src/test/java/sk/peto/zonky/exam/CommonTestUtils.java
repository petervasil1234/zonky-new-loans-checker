package sk.peto.zonky.exam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CommonTestUtils {
	public static String getPathToFileFromResource(String relativePath) {
		return "src/test/resources/" + relativePath;
	}

	public static String readFileAsString(String pathToFile) throws FileNotFoundException {

		StringBuilder sb = new StringBuilder();

		new BufferedReader(new FileReader(pathToFile)).lines().forEach(sb::append);

		return sb.toString();
	}

	public static void removeFile(String pathToFile) {
		new File(pathToFile).delete();
	}
}
