package sk.peto.zonky.exam.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class ConvertUtilsTests {

	@Test
	public void testDateToStringConversionWithNullDate() {

		Assert.assertNull(ConvertUtils.dateToString(null, ""));

		Assert.assertNull(ConvertUtils.dateToString(null, null));
	}

	@Test
	public void testDateToStringConversionWithCorrectValues() {

		// 02/01/1970 7:51:51
		Date date = new Date(111_111_111);
		Assert.assertEquals("1970-01-02 07:51:51", ConvertUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));

		Assert.assertEquals("02/01/1970", ConvertUtils.dateToString(date, "dd/MM/yyyy"));
	}
}
