package sk.peto.zonky.exam.util;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTests {

	@Test
	public void testIsNullOrBlank() {
		Assert.assertTrue(StringUtils.isNullOrBlank(null));

		Assert.assertTrue(StringUtils.isNullOrBlank(""));

		Assert.assertTrue(StringUtils.isNullOrBlank("\n\n \t"));

		Assert.assertFalse(StringUtils.isNullOrBlank("testedString"));
	}

	@Test
	public void testNormalizeUrl() {
		String url = "  http://test.com///  ";

		Assert.assertEquals("http://test.com", StringUtils.normalizeUrl(url));
	}
}
