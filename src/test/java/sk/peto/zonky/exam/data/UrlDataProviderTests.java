package sk.peto.zonky.exam.data;

import org.junit.Assert;
import org.junit.Test;
import sk.peto.zonky.exam.data.exceptions.InvalidParametersException;
import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class UrlDataProviderTests {

	@Test
	public void testValidRequest() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new UrlDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, String> header = new HashMap<>() {
			{
				put("X-Size", "20");
			}
		};

		//TODO replace with some mocked URL Provider
		Map<String, Object> params = new HashMap<>() {
			{
				put("URL", "https://api.zonky.cz/loans/408624");
				put("REQUEST_METHOD", "GET");
				put("HEADER", header);
			}
		};

		String retrievedData = dataProvider.getData(params);

		Assert.assertTrue(retrievedData.contains("408624"));
	}

	@Test
	public void testMissingUrl() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new UrlDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>();

		try {
			dataProvider.getData(params);
		} catch (InvalidParametersException e) {
			assert true;
			return;
		}

		assert false;
	}

	@Test
	public void testInvalidRequestMethod() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new UrlDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>() {
			{
				put("URL", "http://abc.com");
				put("REQUEST_METHOD", "SOME_INVALID_REQUEST_METHOD");
				put("HEADER", new HashMap<>());
			}
		};

		try {
			dataProvider.getData(params);
		} catch (InvalidParametersException e) {
			assert true;
			return;
		}

		assert false;
	}

	@Test
	public void testInvalidHeader() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new UrlDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>() {
			{
				put("URL", "http://abc.com");
				put("REQUEST_METHOD", "GET");
				put("HEADER", "SOME_INVALID_HEADER");
			}
		};

		try {
			dataProvider.getData(params);
		} catch (InvalidParametersException e) {
			assert true;
			return;
		}

		assert false;
	}
}
