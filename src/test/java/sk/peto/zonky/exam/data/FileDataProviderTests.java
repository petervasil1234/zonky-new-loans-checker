package sk.peto.zonky.exam.data;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import sk.peto.zonky.exam.data.exceptions.InvalidParametersException;
import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;
import sk.peto.zonky.exam.entity.Loan;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static sk.peto.zonky.exam.CommonTestUtils.getPathToFileFromResource;

public class FileDataProviderTests {

	@Test
	public void testReadingFromValidFile() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new FileDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>() {
			{
				put("PATH_TO_FILE", getPathToFileFromResource("fileDataProviderSampleData.txt"));
			}
		};

		String expectedData = "sample-data";

		String retrievedData = dataProvider.getData(params);

		Assert.assertEquals(expectedData, retrievedData);
	}

	@Test
	public void fileDataProviderShouldThrowInvalidParametersException() throws UnableToGetDataException {
		DataProvider<String> dataProvider = new FileDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>();

		try {
			dataProvider.getData(params);
		} catch (InvalidParametersException e) {
			assert true;
			return;
		}

		assert false;
	}

	@Test
	public void fileDataProviderShouldThrowUnableToGetDataException() {
		DataProvider<String> dataProvider = new FileDataProvider<>(s -> s, Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>() {
			{
				put("PATH_TO_FILE", getPathToFileFromResource("non-existing-path.txt"));
			}
		};

		try {
			dataProvider.getData(params);
		} catch (UnableToGetDataException e) {
			assert true;
			return;
		}

		assert false;
	}

	@Test
	public void getLoanEntityFromFileAndCompareWithBaseline() throws ParseException, UnableToGetDataException {
		DataProvider<Loan> dataProvider = new FileDataProvider<>(s -> new Gson().fromJson(s, Loan.class), Charset.forName("UTF-8"));

		Map<String, Object> params = new HashMap<>() {
			{
				put("PATH_TO_FILE", getPathToFileFromResource("sample-loan.json"));
			}
		};

		Date expectedLoanDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse("2019-02-22T21:10:55");

		Loan expectedLoan = new Loan(408687, "Auto-moto", expectedLoanDate);

		Loan retrievedLoan = dataProvider.getData(params);

		Assert.assertEquals(expectedLoan, retrievedLoan);

	}
}
