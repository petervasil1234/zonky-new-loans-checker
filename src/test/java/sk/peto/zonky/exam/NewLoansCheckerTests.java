package sk.peto.zonky.exam;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import sk.peto.zonky.exam.configuration.PropertiesConfigurationTests;
import sk.peto.zonky.exam.consumer.ExportLoansToTextFileConsumerTests;
import sk.peto.zonky.exam.consumer.PrintLoansToConsoleConsumerTests;
import sk.peto.zonky.exam.data.FileDataProviderTests;
import sk.peto.zonky.exam.data.UrlDataProviderTests;

/**
 * Suite with all unit tests for NewLoansCheckerApp
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(
	{
		PropertiesConfigurationTests.class,
		ExportLoansToTextFileConsumerTests.class,
		PrintLoansToConsoleConsumerTests.class,
		FileDataProviderTests.class,
		UrlDataProviderTests.class
	}
)
public class NewLoansCheckerTests {
}
