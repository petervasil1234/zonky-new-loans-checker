package sk.peto.zonky.exam.consumer;

import org.junit.Assert;
import org.junit.Test;
import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.util.FormattingUtils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;

public class PrintLoansToConsoleConsumerTests {

	@Test
	public void checkIfArrayOfLoansIsCorrectlyPrintedToConsole() {

		Loan loan1 = new Loan(1, "Sample loan 1", new Date());
		Loan loan2 = new Loan(2, "Sample loan 2", new Date());

		Loan[] loans = new Loan[]{loan1, loan2};

		PrintLoansToConsoleConsumer consumer = new PrintLoansToConsoleConsumer();

		// Create a stream to hold the output
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);

		// Save the default System.out PrintStream
		PrintStream defaultSystemOutPrintStream = System.out;

		// Set System PrintStream to new one
		System.setOut(ps);

		// This consumer should print loans to changed ps PrintStream
		consumer.accept(loans);

		// Revert changes back
		System.out.flush();
		System.setOut(defaultSystemOutPrintStream);

		String expectedText = FormattingUtils.prettyLoanFormatter.apply(loan1) + FormattingUtils.prettyLoanFormatter.apply(loan2);

		String retrievedText = baos.toString().replaceAll("\n", "");

		Assert.assertTrue(retrievedText.contains(expectedText));
	}
}
