package sk.peto.zonky.exam.consumer;

import org.junit.Assert;
import org.junit.Test;
import sk.peto.zonky.exam.CommonTestUtils;
import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.util.FormattingUtils;

import java.io.FileNotFoundException;
import java.util.Date;

public class ExportLoansToTextFileConsumerTests {

	@Test
	public void checkIfArrayOfLoansIsCorrectlyWrittenToFile() throws FileNotFoundException {

		Loan loan1 = new Loan(1, "Sample loan 1", new Date());
		Loan loan2 = new Loan(2, "Sample loan 2", new Date());

		Loan[] loans = new Loan[]{loan1, loan2};

		String pathToFile = CommonTestUtils.getPathToFileFromResource("exportToFileConsumer.txt");

		ExportLoansToTextFileConsumer consumer = new ExportLoansToTextFileConsumer(pathToFile);

		consumer.accept(loans);

		String expectedText = FormattingUtils.prettyLoanFormatter.apply(loan1) + FormattingUtils.prettyLoanFormatter.apply(loan2);

		String retrievedText = CommonTestUtils.readFileAsString(pathToFile);

		//remove temporary file created for this test
		CommonTestUtils.removeFile(pathToFile);

		Assert.assertEquals(expectedText, retrievedText);
	}
}
