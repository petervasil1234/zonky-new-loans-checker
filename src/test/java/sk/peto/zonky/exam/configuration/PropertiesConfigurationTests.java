package sk.peto.zonky.exam.configuration;

import org.junit.Assert;
import org.junit.Test;

public class PropertiesConfigurationTests {

	@Test
	public void checkSingletonDesignPattern() {
		PropertiesConfiguration configuration1 = PropertiesConfiguration.getInstance();
		PropertiesConfiguration configuration2 = PropertiesConfiguration.getInstance();

		Assert.assertSame(configuration1, configuration2);
	}
}
