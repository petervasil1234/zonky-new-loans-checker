package sk.peto.zonky.exam;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.consumer.PrintLoansToConsoleConsumer;
import sk.peto.zonky.exam.data.UrlDataProvider;
import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.task.CheckNewLoansTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static sk.peto.zonky.exam.configuration.Configuration.SchedulerConfiguration.THREAD_POOL_SIZE;
import static sk.peto.zonky.exam.configuration.Configuration.TaskConfiguration.TASK_PERIOD_IN_SECONDS;

/**
 * Application for periodically checking new loans on Zonky marketplace
 */
public class NewLoansCheckerApp {
	public static void main(String[] args) throws InterruptedException {

		Logger logger = LoggerFactory.getLogger(NewLoansCheckerApp.class);

		// Create scheduled executor with fixed number of threads
		logger.info("Creating new scheduler with " + THREAD_POOL_SIZE + " threads ...");
		ScheduledExecutorService executorService = Executors.newScheduledThreadPool(THREAD_POOL_SIZE);


		//Create url data provider which is available to get String data from URL
		//and convert it to other class using passed String -> T converter
		//For converting is used converter from String -> Loan array
		UrlDataProvider<Loan[]> loansDataProvider = new UrlDataProvider<>(s -> new Gson().fromJson(s, Loan[].class));


		// Create task which should be executed by scheduler
		// It requires data provider to get tha data and consumer, which accepts array of loans and can process it
		logger.info("Creating task for checking new loans ...");
		CheckNewLoansTask checkNewLoansTask =
			new CheckNewLoansTask(1, "Check New Loans Task", loansDataProvider, new PrintLoansToConsoleConsumer());


		// Starting CheckNewLoansTask periodically
		// Interval of executing is retrieved from configuration.properties file - TASK_PERIOD_IN_SECONDS
		logger.info("Starting task and executing it each " + TASK_PERIOD_IN_SECONDS + " seconds.");
		executorService.scheduleAtFixedRate(checkNewLoansTask, 0, TASK_PERIOD_IN_SECONDS, TimeUnit.SECONDS);


		// Prevent to stop the program immediately and give it some time for executing
		TimeUnit.HOURS.sleep(1);


		// Stop executing tasks
		executorService.shutdown();
	}
}
