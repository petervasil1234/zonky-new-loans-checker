package sk.peto.zonky.exam.util;

import sk.peto.zonky.exam.entity.Loan;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.function.Function;

/**
 * Purpose is to format objects to better readable form
 */
public class FormattingUtils {

	/**
	 * Format Loan to String which is better readable
	 */
	public static Function<Loan, String> prettyLoanFormatter = loan -> {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return String.format("ID - %-7d| %-20s| %s",
			loan.getId(),
			dateFormat.format(loan.getDatePublished()),
			loan.getName()
		);
	};
}
