package sk.peto.zonky.exam.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * static Convert Utils class with useful methods for conversion of objects to different types or formats
 */
public class ConvertUtils {

	/**
	 * Function for converting Java Date object to specific string
	 *
	 * @param date   date which will be converted to String
	 * @param format format of result String - see allowed formats in SimpleDateFormat
	 * @return formatted date as string in specific format
	 * @see SimpleDateFormat
	 */
	public static String dateToString(Date date, String format) {

		if (date == null) {
			return null;
		}

		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getDefault());
		return df.format(date);
	}

	;
}
