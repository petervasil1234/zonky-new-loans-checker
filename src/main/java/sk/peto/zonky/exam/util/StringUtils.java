package sk.peto.zonky.exam.util;

/**
 * static String Utils class with useful methods for working with String objects
 */
public class StringUtils {

	/**
	 * Check if string is null or blank
	 *
	 * @param s input string which is checked
	 * @return true if String is null or blank (is empty or contains only whitespaces)
	 * false otherwise
	 */
	public static boolean isNullOrBlank(String s) {
		return s == null || s.isBlank();
	}

	/**
	 * Trim url and remove trailing slashes from the end of url
	 *
	 * @param url url
	 * @return return trimmed url without trailing slashes
	 */
	public static String normalizeUrl(String url) {
		if (url != null) {

			url = url.trim();

			while (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
		}

		return url;
	}
}
