package sk.peto.zonky.exam.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.configuration.exceptions.UnableToLoadPropertyFileException;
import sk.peto.zonky.exam.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class with properties from external configuration
 * Implemented using Singleton design pattern with double checked locking technique to reduce synchronization time
 */
public class PropertiesConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(PropertiesConfiguration.class);
	/**
	 * variable which handle single instance of PropertiesConfiguration class
	 * marked as volatile to avoid caching by CPUs, so it is still read from main memory
	 */
	private static volatile PropertiesConfiguration propertiesConfiguration;
	private Properties properties;

	private PropertiesConfiguration() {
		this.properties = new Properties();

		String pathToConfigFile = Configuration.CONFIGURATION_FILE_PATH;

		InputStream is = this.getClass().getClassLoader().getResourceAsStream(pathToConfigFile);

		if (is != null) {
			try {
				this.properties.load(is);
			} catch (IOException e) {
				logger.error("Unable to load property file " + pathToConfigFile);
				logger.error(e.getMessage());
				throw new UnableToLoadPropertyFileException("Unable to load property file " + pathToConfigFile);
			}
		}
	}

	/**
	 * Static method to get instance of PropertiesConfiguration class
	 * Implemented with double checked locking - it reduce times for synchronization because it synchronizes
	 * only if first check for null is true and it is only during first call of getInstance method
	 *
	 * @return PropertiesConfiguration singleton instance
	 */
	public static PropertiesConfiguration getInstance() {
		if (propertiesConfiguration == null) {
			synchronized (PropertiesConfiguration.class) {
				if (propertiesConfiguration == null) {
					propertiesConfiguration = new PropertiesConfiguration();
				}
			}
		}
		return propertiesConfiguration;
	}

	/**
	 * Retrieve value from properties object
	 *
	 * @param key key which will be searched in properties object
	 * @return return value for specific key as String or null if properties object doesnt contains value for given key
	 */
	public String get(String key) {
		return this.properties.getProperty(key);
	}

	/**
	 * Retrieve value from properties object
	 *
	 * @param key          key which will be searched in properties object
	 * @param defaultValue value which is returned if key is not found in properties object
	 * @return return trimmed value for specific key as String
	 * or defaultValue if properties object doesnt contains value for given key or value for given key is blank
	 */
	public String getOrDefault(String key, String defaultValue) {

		String value = "";

		if (this.properties.containsKey(key)) {
			value = get(key);
		}

		return !StringUtils.isNullOrBlank(value) ? get(key).trim() : defaultValue;
	}

}
