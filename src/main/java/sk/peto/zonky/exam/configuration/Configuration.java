package sk.peto.zonky.exam.configuration;

import sk.peto.zonky.exam.task.CheckNewLoansTask;
import sk.peto.zonky.exam.util.StringUtils;

import java.nio.charset.Charset;

/**
 * Configuration parameters of the application
 * Centralizing all configuration parameters used in application to avoid hardcoded values on many places in code
 */
public class Configuration {

	/**
	 * Relative path to external configuration properties file which can contains some external parameters
	 */
	public static final String CONFIGURATION_FILE_PATH = "./configuration.properties";

	/**
	 * Specific inner static class for Encoding configuration
	 */
	public static class Encoding {
		public static final Charset CHARSET = Charset.forName("UTF-8");
	}

	/**
	 * Specific inner static class for Scheduler configuration
	 */
	public static class SchedulerConfiguration {

		/**
		 * Thread pool size is used as number of fixed threads which scheduler service can use
		 * Value is read from file specified in CONFIGURATION_FILE_PATH variable for THREAD_POOL_SIZE key
		 * If value is not present in this file or is null or blank, default value 1 is used
		 * This value should be at least 1
		 * If this value is 1, scheduler service will not be able to perform tasks parallel,
		 * so it means that each next scheduled task need to wait until all previously scheduled tasks are finished
		 */
		public static final int THREAD_POOL_SIZE = Integer.parseInt(
			PropertiesConfiguration.getInstance().getOrDefault("THREAD_POOL_SIZE", "1")
		);
	}

	/**
	 * Specific inner static class for Task configuration
	 */
	public static class TaskConfiguration {

		/**
		 * Task period in seconds parameter is used as period in seconds for task
		 * It means that task will execute repeatedly each TASK_PERIOD_IN_SECONDS seconds
		 * Value is read from file specified in CONFIGURATION_FILE_PATH variable for TASK_PERIOD_IN_SECONDS key
		 * If value is not present in this file or is null, default value 5 seconds is used
		 * This value should be at least 1 to make sense
		 */
		public static final int TASK_PERIOD_IN_SECONDS = Integer.parseInt(
			PropertiesConfiguration.getInstance().getOrDefault("TASK_PERIOD_IN_SECONDS", "5")
		);

		/**
		 * Url for providing loans on Zonky marketplace
		 * This url is used in CheckNewLoansTask and is passing to UrlDataProvider to get loans from Zonky API
		 * Value is read from file specified in CONFIGURATION_FILE_PATH variable for LOANS_URL key
		 *
		 * @see CheckNewLoansTask
		 */
		public static final String LOANS_URL = StringUtils.normalizeUrl(PropertiesConfiguration.getInstance().get("LOANS_URL"));

		/**
		 * Parameter which specifies maximum count of loans to retrieve from Zonky API in one request
		 * This value is used in CheckNewLoansTask and is passing to UrlDataProvider request header to specify
		 * maximum of loans to retrieve from Zonky API
		 * Value is read from file specified in CONFIGURATION_FILE_PATH variable for MAX_LOANS_TO_GET key
		 * If value is not present in this file or is null or blank, default value 20 is used
		 *
		 * @see CheckNewLoansTask
		 */
		public static final int MAX_LOANS_TO_GET = Integer.parseInt(
			PropertiesConfiguration.getInstance().getOrDefault("MAX_LOANS_TO_GET", "20")
		);


		/**
		 * Parameter which specifies format of date used as query parameter for Zonky API requests
		 * Value is read from file specified in CONFIGURATION_FILE_PATH variable for API_DATE_FILTER_FORMAT key
		 * If value is not present in this file or is null or blank, default value "yyyy-MM-dd'T'HH:mm:ss" is used
		 *
		 * @see CheckNewLoansTask
		 */
		public static final String API_DATE_FILTER_FORMAT = PropertiesConfiguration.getInstance()
			.getOrDefault("API_DATE_FILTER_FORMAT", "yyyy-MM-dd'T'HH:mm:ss");
	}
}
