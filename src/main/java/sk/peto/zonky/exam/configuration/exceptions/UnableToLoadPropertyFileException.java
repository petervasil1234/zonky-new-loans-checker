package sk.peto.zonky.exam.configuration.exceptions;

/**
 * Unchecked exception which is thrown if path to configuration file is specified but there is
 * problem with reading this file or file doesnt exists
 */
public class UnableToLoadPropertyFileException extends RuntimeException {

	public UnableToLoadPropertyFileException(String message) {
		super(message);
	}
}
