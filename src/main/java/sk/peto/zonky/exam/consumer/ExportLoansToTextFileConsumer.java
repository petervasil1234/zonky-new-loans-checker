package sk.peto.zonky.exam.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.util.FormattingUtils;
import sk.peto.zonky.exam.util.StringUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.function.Consumer;

/**
 * Consumer which exports all loans in array to specified file
 * Path to file is specified in constructor
 * For export specific loans is used Loan.toString function
 *
 * @see Loan
 */
public class ExportLoansToTextFileConsumer implements Consumer<Loan[]> {

	private static final Logger logger = LoggerFactory.getLogger(ExportLoansToTextFileConsumer.class);

	/**
	 * Charset used for writing to file
	 */
	private Charset charset;

	/**
	 * Path to file where are data written
	 * If null or blank, writing to file is ignored
	 */
	private String pathToFile;

	/**
	 * Constructs ExportLoansToTextFileConsumer with the specified path to file and default charset UTF-8
	 *
	 * @param pathToFile path to file where are data written
	 * @see Charset
	 */
	public ExportLoansToTextFileConsumer(final String pathToFile) {
		this.pathToFile = pathToFile;
		this.charset = Charset.forName("UTF-8");
	}

	/**
	 * Constructs ExportLoansToTextFileConsumer with the specified path to file and charset
	 *
	 * @param pathToFile path to file where are data written
	 * @param charset    charset
	 * @see Charset
	 */
	public ExportLoansToTextFileConsumer(final String pathToFile, final Charset charset) {
		this.charset = charset;
		this.pathToFile = pathToFile;
	}

	/**
	 * Accepts array of loans and print them to System.out console
	 *
	 * @param loans array of loans
	 */
	@Override
	public void accept(final Loan[] loans) {
		if (loans != null && loans.length > 0 && !StringUtils.isNullOrBlank(pathToFile)) {
			try (BufferedWriter out = new BufferedWriter(new FileWriter(pathToFile, this.charset, true))) {
				for (Loan loan : loans) {
					out.append(FormattingUtils.prettyLoanFormatter.apply(loan)).append("\n");
				}
			} catch (IOException e) {
				logger.error("Problem with writing to file " + pathToFile);
			}
		}
	}

}
