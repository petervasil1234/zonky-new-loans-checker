package sk.peto.zonky.exam.consumer;

import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.util.FormattingUtils;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Consumer which print all loans in array to System.out console
 * For printing specific loans is used Loan.toString function
 *
 * @see Loan
 */
public class PrintLoansToConsoleConsumer implements Consumer<Loan[]> {

	/**
	 * Accepts array of loans and print them to System.out console
	 *
	 * @param loans array of loans
	 */
	@Override
	public void accept(final Loan[] loans) {
		if (loans != null) {
			if (loans.length > 0) {
				System.out.println("\n================================ NEW LOANS ================================\n");

				Arrays.stream(loans).forEach(loan ->
					System.out.println(FormattingUtils.prettyLoanFormatter.apply(loan))
				);

				System.out.println("\n===========================================================================\n");
			}
		}
	}
}
