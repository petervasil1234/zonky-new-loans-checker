package sk.peto.zonky.exam.task.exceptions;

/**
 * Unchecked exception which is thrown if parameters in task are in incorrect format or required parameters are missing
 * or are null
 */
public class InvalidTaskParametersException extends RuntimeException {

	public InvalidTaskParametersException(String message) {
		super(message);
	}
}
