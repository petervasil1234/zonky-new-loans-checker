package sk.peto.zonky.exam.task;

import java.util.function.Consumer;

/**
 * Basic immutable abstract task which implements Runnable and accepts some Consumer to consume and process some data of type T
 *
 * @param <T> type of data to process by consumer
 * @see CheckNewLoansTask
 */
public abstract class ConsumerTask<T> implements Runnable {

	/**
	 * Id of task
	 */
	private int id;

	/**
	 * Name of task
	 */
	private String name;

	/**
	 * Consumer - it is something like callback function, which can process data of type T
	 */
	private Consumer<T> consumer;

	/**
	 * Constructs ConsumerTask with the specified id, name and consumer
	 *
	 * @param id       id of task
	 * @param name     name of task
	 * @param consumer consumer which accepts data of type T and process them
	 */
	public ConsumerTask(int id, String name, Consumer<T> consumer) {
		this.id = id;
		this.name = name;
		this.consumer = consumer;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Consumer<T> getConsumer() {
		return consumer;
	}
}
