package sk.peto.zonky.exam.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.configuration.Configuration;
import sk.peto.zonky.exam.data.DataProvider;
import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;
import sk.peto.zonky.exam.entity.Loan;
import sk.peto.zonky.exam.task.exceptions.InvalidTaskParametersException;
import sk.peto.zonky.exam.util.ConvertUtils;
import sk.peto.zonky.exam.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static sk.peto.zonky.exam.configuration.Configuration.TaskConfiguration.API_DATE_FILTER_FORMAT;
import static sk.peto.zonky.exam.configuration.Configuration.TaskConfiguration.MAX_LOANS_TO_GET;

/**
 * Specific consumer task which is implementation of ConsumerTask<Loan[]>
 * Purpose is to check if there are some new loans on the Zonky marketplace
 */
public class CheckNewLoansTask extends ConsumerTask<Loan[]> {

	private static final Logger logger = LoggerFactory.getLogger(CheckNewLoansTask.class);

	/**
	 * Basic url which is used for retrieving the data from UrlDataProvider
	 * In the run method is combined with specific date filter which is passed as query param to this url
	 * Cannot be null or blank
	 */
	private String url;

	/**
	 * Consumer which accepts array of Loans and process it
	 */
	private Consumer<Loan[]> consumer;

	/**
	 * Date which is appended to url in run method as query param to retrieve only loans published from specific date
	 * First value is set to date of creating instance of CheckNewLoansTask, each next value is set to last execution date
	 * of run method
	 */
	private Date publishedDateThreshold;

	/**
	 * Data provider for retrieving data
	 * This task is limited to use specific UrlDataProvider due to business logic inside of task
	 */
	private DataProvider<Loan[]> dataProvider;

	/**
	 * Constructs CheckNewLoansTask with the specified id, name, dateProvider and consumer
	 *
	 * @param id           id of task
	 * @param name         name of task
	 * @param dataProvider url data provider which provides data from datasource
	 * @param consumer     consumer which accepts array of Loans
	 * @throws InvalidTaskParametersException if dataProvider or consumer are null
	 *                                        or if url from Configuration.TaskConfiguration.LOANS_URL is null or blank
	 */
	public CheckNewLoansTask(final int id, final String name, final DataProvider<Loan[]> dataProvider, final Consumer<Loan[]> consumer) {
		super(id, name, consumer);
		this.consumer = consumer;
		this.dataProvider = dataProvider;
		this.publishedDateThreshold = new Date();
		this.url = Configuration.TaskConfiguration.LOANS_URL;
		validateTaskParameters();
	}

	/**
	 * Execute task due to specified business logic
	 * Retrieve loans from Zonky API which were published (publishedDate) later than last execution of this method
	 * If it is unable to get data from datasource, error message is logged but program will continue without crashing
	 */
	@Override
	public void run() {
		String dateFilter = "?datePublished__gt=" + ConvertUtils.dateToString(publishedDateThreshold, API_DATE_FILTER_FORMAT);

		publishedDateThreshold = new Date();

		String urlWithFilter = this.url + dateFilter;

		logger.debug("TASK - " + getName() + " - Checking new Loans on " + urlWithFilter);


		Map<String, Object> params = new HashMap<>();
		params.put("URL", urlWithFilter);
		params.put("HEADER", getHeader());

		Loan[] loans;

		try {
			loans = dataProvider.getData(params);

			if (loans != null) {
				this.consumer.accept(loans);
			}

		} catch (UnableToGetDataException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Header which is send to data provider
	 *
	 * @return Hash Map of key value pairs which are used as header in request
	 */
	private Map<String, String> getHeader() {
		return new HashMap<>() {
			{
				put("X-Size", String.valueOf(MAX_LOANS_TO_GET));
			}
		};
	}

	/**
	 * Validate if instance of task was created with correct params
	 *
	 * @throws InvalidTaskParametersException if dataProvider or consumer are null
	 *                                        or if url from Configuration.TaskConfiguration.LOANS_URL is null or blank
	 */
	private void validateTaskParameters() {

		String validationMessage = "";

		if (this.dataProvider == null) {
			validationMessage = "Data provider cannot be null.";
		} else if (consumer == null) {
			validationMessage = "Consumer of retrieved data cannot be null.";
		} else if (StringUtils.isNullOrBlank(this.url)) {
			validationMessage = "Url for retrieving loans from API cannot be null or blank.";
		}

		if (!validationMessage.isEmpty()) {
			logger.error(validationMessage);
			throw new InvalidTaskParametersException(validationMessage);
		}
	}
}
