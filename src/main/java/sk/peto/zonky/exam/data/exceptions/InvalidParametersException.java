package sk.peto.zonky.exam.data.exceptions;

/**
 * Unchecked exception which is thrown if map of parameters provided to data provider has not
 * valid values for actual data provider
 * The specific problem should be specified in the message
 */
public class InvalidParametersException extends RuntimeException {

	public InvalidParametersException(String message) {
		super(message);
	}
}
