package sk.peto.zonky.exam.data.exceptions;

/**
 * Checked exception which is thrown if data provider has problem with retrieving tha data from datasource
 * The specific problem should be specified in the message
 */
public class UnableToGetDataException extends Exception {

	public UnableToGetDataException(String message) {
		super(message);
	}
}
