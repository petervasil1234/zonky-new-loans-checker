package sk.peto.zonky.exam.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.data.exceptions.InvalidParametersException;
import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;
import sk.peto.zonky.exam.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * File data provider based implementation of the AbstractDataProvider
 * Returns data which are read from the file in the specific type of T
 *
 * @param <T> type of retrieved object
 */
public class FileDataProvider<T> extends AbstractDataProvider<String, T> {

	private static final Logger logger = LoggerFactory.getLogger(FileDataProvider.class);

	/**
	 * Constructs FileDataProvider with the specified converter and default charset
	 *
	 * @param converter mapper function from String to T
	 */
	public FileDataProvider(final Function<String, ? extends T> converter) {
		super(converter);
	}

	/**
	 * Constructs FileDataProvider with the specified converter and charset
	 *
	 * @param converter mapper function from String to T
	 * @param charset   charset
	 * @see Charset
	 */
	public FileDataProvider(final Function<String, ? extends T> converter, final Charset charset) {
		super(converter, charset);
	}

	/**
	 * Retrieve data from the file with path specified in params map as PATH_TO_FILE key
	 *
	 * @param params map of parameters which can be passed to the data provider
	 * @return data of T type converted from String to T using converter
	 * @throws NullPointerException     if the converter is null
	 * @throws UnableToGetDataException if it is not possible to read from the file or if the file not exists
	 */
	@Override
	public T getData(final Map<String, Object> params) throws UnableToGetDataException {

		T items;

		validateParams(params);

		String pathToFile = (String) params.get("PATH_TO_FILE");

		File file = new File(pathToFile);

		try (BufferedReader br = new BufferedReader(new FileReader(file, this.charset))) {

			String data = br.lines().collect(Collectors.joining());
			items = converter.apply(data);

		} catch (IOException e) {
			logger.error("Unable to get data from " + pathToFile);
			logger.error(e.getMessage());
			throw new UnableToGetDataException(e.getMessage());
		}

		return items;
	}

	/**
	 * Validate if there are all required parameters in the map and if they are in required format
	 *
	 * @param params parameters passed to getData function
	 * @throws InvalidParametersException if params map not contains PATH_TO_FILE or if PATH_TO_FILE
	 *                                    is not in String format or if it is blank string
	 */
	protected void validateParams(final Map<String, Object> params) {

		String validationMessage = "";

		if (!params.containsKey("PATH_TO_FILE")) {
			validationMessage = "Parameters map for retrieving data has to contain PATH_TO_FILE parameter.";
		} else {
			Object pathToFileFromMap = params.get("PATH_TO_FILE");

			if (!(pathToFileFromMap instanceof String)) {
				validationMessage = "Parameter PATH_TO_FILE has to be String.";
			} else if (StringUtils.isNullOrBlank((String) pathToFileFromMap)) {
				validationMessage = "Parameter PATH_TO_FILE cannot be null or blank.";
			}
		}

		if (!validationMessage.isEmpty()) {
			logger.error(validationMessage);
			throw new InvalidParametersException(validationMessage);
		}
	}
}
