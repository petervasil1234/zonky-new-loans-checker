package sk.peto.zonky.exam.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.peto.zonky.exam.data.exceptions.InvalidParametersException;
import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;
import sk.peto.zonky.exam.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Url data provider based implementation of the AbstractDataProvider
 * Returns data of type T which are read from the specific URL and converted from String to T using converter
 *
 * @param <T> type of retrieved object
 */
public class UrlDataProvider<T> extends AbstractDataProvider<String, T> {

	private static final Logger logger = LoggerFactory.getLogger(UrlDataProvider.class);

	/**
	 * Constructs UrlDataProvider with the specified converter and default charset
	 *
	 * @param converter mapper function from String to T
	 */
	public UrlDataProvider(final Function<String, ? extends T> converter) {
		super(converter);
	}

	/**
	 * Constructs UrlDataProvider with the specified converter and charset
	 *
	 * @param converter mapper function from String to T
	 * @param charset   charset
	 * @see Charset
	 */
	public UrlDataProvider(final Function<String, ? extends T> converter, final Charset charset) {
		super(converter, charset);
	}

	/**
	 * Retrieve data from the URL with specified in params map as URL key
	 * Optional parameter in params map is REQUEST_METHOD - it is HTTP request method - default is GET
	 * Optional parameter in params map is HEADER - it has to have type Map<String, String>
	 *
	 * @param params map of parameters which can be passed to the data provider
	 * @return data of T type converted from String to T using converter
	 * @throws NullPointerException     if the converter is null
	 * @throws UnableToGetDataException if it is not possible to read from the URL or if response code is not equals 200
	 */
	@Override
	public T getData(final Map<String, Object> params) throws UnableToGetDataException {

		T items;

		validateParams(params);

		String url = (String) params.get("URL");

		String requestMethod = (String) params.getOrDefault("REQUEST_METHOD", "GET");

		Map<String, String> header = (Map<String, String>) params.getOrDefault("HEADER", new HashMap<>());

		try {

			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

			con.setRequestMethod(requestMethod);
			setHeader(con, header);

			if (con.getResponseCode()!=200) {
				logger.error("Unable to get data from " + url + " - Response code " + con.getResponseCode());
				throw new UnableToGetDataException("Unable to get data from " + url + " - Response code " + con.getResponseCode());
			}

			try (InputStream is = con.getInputStream();
				 BufferedReader rd = new BufferedReader(new InputStreamReader(is, this.charset))
			) {
				String data = rd.lines().collect(Collectors.joining());
				items = converter.apply(data);

			} catch (IOException e) {
				logger.error("Unable to get data from " + url);
				logger.error(e.getMessage());
				throw new UnableToGetDataException(e.getMessage());
			}
		} catch (IOException e) {
			logger.error("Unable to open connection for URL " + url);
			logger.error(e.getMessage());
			throw new UnableToGetDataException(e.getMessage());
		}

		return items;
	}

	/**
	 * Validate if there are all required parameters in the map and if they are in required format
	 *
	 * @param params parameters passed to getData function
	 * @throws InvalidParametersException if params map not contains URL or if URL
	 *                                    is not in String format or if it is blank string
	 *                                    or if HEADER parameter is not Map<String, String>
	 *                                    or if REQUEST_METHOD parameter is not one of the
	 *                                    following values: GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE
	 */
	@Override
	protected void validateParams(final Map<String, Object> params) {

		StringBuilder validationMessageBuilder = new StringBuilder();

		validationMessageBuilder
			.append(validateUrlParam(params))
			.append(validateRequestMethodParam(params))
			.append(validateRequestHeaderParam(params));

		if (!validationMessageBuilder.toString().isBlank()) {
			logger.error(validationMessageBuilder.toString());
			throw new InvalidParametersException(validationMessageBuilder.toString());
		}
	}

	private String validateRequestHeaderParam(final Map<String, Object> params) {
		String validationMessage = "";

		if (params.containsKey("HEADER")) {

			Object header = params.get("HEADER");

			if (!(header instanceof Map)) {
				validationMessage = "Request method has to be Map<String, String>.";
			}
		}

		return validationMessage;
	}

	private String validateRequestMethodParam(final Map<String, Object> params) {
		String validationMessage = "";

		Object requestMethod = params.getOrDefault("REQUEST_METHOD", "GET");

		if (!(requestMethod instanceof String)) {
			validationMessage = "Request method has to be String value.";
		} else {
			switch ((String) requestMethod) {
				case "GET":
				case "POST":
				case "HEAD":
				case "OPTIONS":
				case "PUT":
				case "DELETE":
				case "TRACE":
					break;
				default:
					validationMessage = "Request method has to be one of the following values: " +
						"GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE.";

			}
		}

		return validationMessage;
	}

	private String validateUrlParam(final Map<String, Object> params) {
		String validationMessage = "";

		if (!params.containsKey("URL")) {
			validationMessage = "Parameters map for retrieving data has to contain URL parameter.";
		} else {

			Object url = params.get("URL");

			if (!(url instanceof String)) {
				validationMessage = "Parameter URL has to be String.";
			} else if (StringUtils.isNullOrBlank((String) url)) {
				validationMessage = "URL parameter cannot be null or blank.";
			}
		}
		return validationMessage;
	}

	private void setHeader(final HttpURLConnection con, final Map<String, String> headers) {
		if (headers!=null) {
			for (Map.Entry<String, String> header : headers.entrySet()) {
				con.setRequestProperty(header.getKey(), header.getValue());
			}
		}
	}
}
