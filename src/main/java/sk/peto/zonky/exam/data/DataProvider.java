package sk.peto.zonky.exam.data;

import sk.peto.zonky.exam.data.exceptions.UnableToGetDataException;

import java.util.Map;

/**
 * Interface for retrieving generic kind of data from different sources
 *
 * @param <T> type of retrieved object (e.g. String, Map ...)
 * @see AbstractDataProvider
 * @see FileDataProvider
 * @see UrlDataProvider
 */
public interface DataProvider<T> {

	/**
	 * Retrieve data of specific type T
	 *
	 * @param params map of parameters which can be passed to the data provider
	 * @return data of generic type T
	 * @throws UnableToGetDataException if there is problem with retrieving data
	 */
	T getData(Map<String, Object> params) throws UnableToGetDataException;
}
