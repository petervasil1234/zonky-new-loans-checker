package sk.peto.zonky.exam.data;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.function.Function;

/**
 * Abstract implementation of the DataProvider
 * Extending allows create implementation of data provider which
 * returns object of type T by transforming from V using converter from V -> T
 * Converter from V -> T is function, which accepts object of V type and transform it to T type
 *
 * @param <T> type of retrieved object
 */
public abstract class AbstractDataProvider<V, T> implements DataProvider<T> {

	/**
	 * Charset used for reading from input streams
	 */
	protected Charset charset;

	/**
	 * Mapper function from V to T, which should convert input data of V type to T data type
	 */
	protected Function<V, ? extends T> converter;

	/**
	 * AbstractDataProvider with the specified converter and default charset
	 *
	 * @param converter mapper function from V to T
	 */
	protected AbstractDataProvider(final Function<V, ? extends T> converter) {
		this.converter = converter;
		this.charset = Charset.defaultCharset();
	}

	/**
	 * AbstractDataProvider with the specified converter and charset
	 *
	 * @param converter mapper function from V to T
	 * @param charset   charset
	 * @see Charset
	 */
	protected AbstractDataProvider(final Function<V, ? extends T> converter, final Charset charset) {
		this.charset = charset;
		this.converter = converter;
	}

	/**
	 * Validate if there are all required parameters in the map and if they are in required format
	 *
	 * @param params parameters passed to getData function
	 */
	protected abstract void validateParams(final Map<String, Object> params);
}
