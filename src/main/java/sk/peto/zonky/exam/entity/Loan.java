package sk.peto.zonky.exam.entity;

import java.util.Date;
import java.util.Objects;

/**
 * Immutable entity for Loan from Zonky API
 * It is simplified version of Zonky API Loan entity which contains only 3 fields - id, name and datePublished
 */
public class Loan {
	private long id;
	private String name;
	private Date datePublished;

	public Loan() {
	}

	public Loan(long id, String name, Date datePublished) {
		this.id = id;
		this.name = name;
		this.datePublished = datePublished;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getDatePublished() {
		return datePublished;
	}

	@Override
	public String toString() {
		return "Loan{" +
			"id=" + id +
			", name='" + name + '\'' +
			", datePublished=" + datePublished +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Loan loan = (Loan) o;

		if (id != loan.id) return false;
		if (!Objects.equals(name, loan.name)) return false;
		if (this.datePublished == null && loan.datePublished == null) {
			return true;
		} else if (this.datePublished == null || loan.datePublished == null) {
			return false;
		} else return this.datePublished.toString().equals(loan.datePublished.toString());
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}
}
