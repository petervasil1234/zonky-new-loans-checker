# Zonky Loans Checker

__Zonky Loans Checker__ is simple Java application whose 
purpose is to periodically check the loans on the Zonky marketplace.

If there are some new loans they are printed to the console by default.

## Building the project
### Prerequsities
- __Maven 3.3.9+__ installed and set in __$PATH__

To build the application you need to open 
__cmd__, if you use Windows environment 
or __Terminal__, if you use UNIX based environment, navigate to 
project folder where __pom.xml__ file is located and 
run the command `mvn clean package`. 
If everything is built successfully there will be 
__ZonkyLoansChecker__ folder and __ZonkyLoansChecker.zip__ located inside the __target__ folder.   

### Content of built folder
- __configuration.properties__ - file for settings the main configuration. 
Settings in this file are described in section 
[Main configuration](#markdown-header-main-configuration).

- __simplelogger.properties__ - file for settings the logging configuration.
Settings in this file are described in section
[Logging configuration](#markdown-header-logging-configuration).

- __lib__ - folder with external dependencies jar files which 
are required to run the application correctly.

- __ZonkyLoansChecker.jar__ - executable jar file.

## Run the application
### Prerequsities
- __Java 11+ version__ installed and set in __$PATH__


For starting application you need to open 
__cmd__, if you use Windows environment 
or __Terminal__, if you use UNIX based environment, navigate to 
folder __ZonkyLoansChecker__ which was built by Maven and run the command
`java -jar ZonkyLoansChecker.jar`
 
## Configuration
### Main configuration
You can change the default configuration by settings in __configuration.properties__ file
which is located inside the built __ZonkyLoansChecker__ folder.
There are few changeable parameters:

- __THREAD_POOL_SIZE__ - 
is used as number of fixed threads which scheduler service can use.
This value should be at least 1.
If this value is 1, scheduler service will not be able to perform tasks in parallel,
so it means that each next scheduled task need to wait until all previously scheduled tasks are finished.
If value is not present in this file or is null or blank, default value 1 is used.

- __TASK_PERIOD_IN_SECONDS__ - 
is used as period in seconds for task.
It means that task will execute repeatedly each TASK_PERIOD_IN_SECONDS seconds.
This value should be at least 1 to make sense.
If value is not present in this file or is null or blank, default value 300 seconds (5 minutes) is used.

- __LOANS_URL__ - 
Url for providing loans on Zonky marketplace.
This url is used for CheckNewLoansTask.
This parameter is __mandatory__ and has no default value.
In the future this parameter could be replaced by BASE_URL and specific endpoints like LOANS_ENDPOINT and so on.

- __API_DATE_FILTER_FORMAT__ - 
Parameter which specifies format of date used as query parameter for Zonky API requests.
E.g. in url <https://api.zonky.cz/loans/marketplace?datePublished__gt=yyyy-MM-dd'T'HH:mm:ss>
If value is not present in this file or is null or blank, default value `yyyy-MM-dd'T'HH:mm:ss` is used.

### Logging configuration
You can change the default configuration for logging into console by settings in __simplelogger.properties__ file
which is located inside the built __ZonkyLoansChecker__ folder.
All changeable attributes are mentioned and described in this file.
